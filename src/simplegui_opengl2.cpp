#include "shared.h"
#include "simplegui_opengl2.h"
#include <thread>


// [Win32] Our example includes a copy of glfw3.lib pre-compiled with VS2010 to maximize ease of testing and compatibility with old VS compilers.
// To link with VS2010-era libraries, VS2015+ requires linking with legacy_stdio_definitions.lib, which we do using this pragma.
// Your own project should not be affected, as you are likely to link with a newer binary of GLFW that is adequate for your version of Visual Studio.
#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS)
#pragma comment(lib, "legacy_stdio_definitions")
#endif

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}



SimpleGuiOpenGL2::SimpleGuiOpenGL2( const int w, const int h) {
	width = w;
	height = h;

	Init();
}


SimpleGuiOpenGL2::~SimpleGuiOpenGL2() {
	Cleanup();
	 
	delete[] tex_data_;
	tex_data_ = nullptr;
}

int SimpleGuiOpenGL2::Init () {

    // Setup window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;
    window = glfwCreateWindow(1280, 720, "RayTracer & Matěj Nevlud 2020", NULL, NULL);
    if (window == NULL)
        return 1;
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL2_Init();


    // Our state
    tex_data_ = new float[width * height * 4 * sizeof( float )];	
	CreateTexture();

	return 0;

}

int SimpleGuiOpenGL2::Cleanup()
{
	// Cleanup
    ImGui_ImplOpenGL2_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();

	return 0;
}

// abstract method reimplemented in the descendant
int SimpleGuiOpenGL2::Ui()
{
	return 0;
}

Color4f SimpleGuiOpenGL2::get_pixel( const int x, const int y, const float t )
{
	return Color4f{ 1.0f, 0.0f, 1.0f, 1.0f };
}


void SimpleGuiOpenGL2::Producer()
{
    float * local_image_data = new float[width * height * 4];
    int local_frame_counter = 0;
    while(true){
        //4 stands for RGBA
        
        int t = 0;
#pragma omp parallel for schedule(dynamic)
        for ( int y = 0; y < height; ++y ){		
            for ( int x = 0; x < width; ++x ) {				
                const Color4f pixel = get_pixel(x, y, t);
                const int offset = ( y * width + x ) * 4;

                local_image_data[offset] = pixel.r;
                local_image_data[offset + 1] = pixel.g;
                local_image_data[offset + 2] = pixel.b;
                local_image_data[offset + 3] = pixel.a;
                //pixel.copy( local_image_data[offset] );
            }
        }
        memcpy( tex_data_, local_image_data, width * height * 4 * sizeof( float ) );
        //memcpy( frame_counter, &local_frame_counter, sizeof( local_frame_counter ) );
        local_frame_counter++;
        frame_counterS++;
    }
    delete[] local_image_data;
}


int SimpleGuiOpenGL2::MainLoop()
{
	// start image producing threads
	std::thread producer_thread( &SimpleGuiOpenGL2::Producer, this );
	//bool r = setThreadPriority( producer_thread.native_handle(), THREAD_PRIORITY_BELOW_NORMAL );

    
	// Main loop
    while (!glfwWindowShouldClose(window)) {
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL2_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();



        // Use a Begin/End pair to created a named window
        ImGui::Begin( "Ray Tracer Params" );
        Ui();
        //ImGui::Text( "Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate );
        ImGui::Text( "Raytracer frame no. %d", frame_counterS);
	    ImGui::End();



        //CreateTexture();
        //Producer();
        UpdateTexture();
        ImGui::Begin( "Raytraced Result", 0, ImGuiWindowFlags_AlwaysAutoResize );
		ImGui::Image((void *)(intptr_t) image_texture , ImVec2( float( width ), float( height ) ) );
		ImGui::End();
        
        // Rendering
        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        //glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);

        // If you are using this code with non-legacy OpenGL header/contexts (which you should not, prefer using imgui_impl_opengl3.cpp!!),
        // you may need to backup/reset/restore current shader using the commented lines below.
        //GLint last_program;
        //glGetIntegerv(GL_CURRENT_PROGRAM, &last_program);
        //glUseProgram(0);
        ImGui_ImplOpenGL2_RenderDrawData(ImGui::GetDrawData());
        //glUseProgram(last_program);

        glfwMakeContextCurrent(window);
        glfwSwapBuffers(window);

        
        glfwSwapInterval((vsync_)? 1 : 0);
        
    }
	//finish_request_.store( true, std::memory_order_release );
	//producer_thread.join();

	return 0;
}


void SimpleGuiOpenGL2::CreateTexture()
{
	// Create a OpenGL texture identifier
        //if(image_texture) delete image_texture;
        glGenTextures(1, &image_texture);
        glBindTexture(GL_TEXTURE_2D, image_texture);

        // Setup filtering parameters for display
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // Upload pixels into texture
        //glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
        //glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_FLOAT, tex_data_);

}

void SimpleGuiOpenGL2::UpdateTexture() {
        // Upload pixels into texture
        glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_FLOAT, tex_data_);
}